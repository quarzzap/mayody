class ChaptersController < ApplicationController
	def new
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.build
	end

	def create
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.create(chapter_params)
		redirect_to [@course, @chapter]
	end

	def show
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:id])
	end

	private
		def chapter_params
			params.require(:chapter).permit(:name, :description)
		end
end
