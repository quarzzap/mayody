
class TestsController < ApplicationController
	def show
		@test = Test.find(params[:id])
	end

	def start
		session["test#{params[:id]}"] = []
		puts "test#{params[:id]}"
		redirect_to "/tests/#{params[:id]}/run/0"
	end

	def question
		@test = Test.find(params[:id])
		@number = Integer(params[:number])
		@question = @test.questions.to_a[@number]
		@options = @question.options.to_a
	end

	def save_answer
		@test = Test.find(params[:id])
		@number = Integer(params[:number])
		@answer = Integer(params[:selected])
		session["test#{params[:id]}"][@number] = @answer
		if @number < @test.questions.count - 1
			redirect_to "/tests/#{params[:id]}/run/#{@number + 1}"
		else
			redirect_to "/tests/#{params[:id]}/done"
		end
	end

	def finish
		return if not logged_in?
		if not session["test#{params[:id]}"].nil?
			@in_test = true
		else
			@in_test = false
			return
		end
		@test = Test.find(params[:id])
		@right = 0
		@test.questions.to_a.each_with_index do |q, i|
			(@right += 1) if q.answer == session["test#{params[:id]}"][i]
		end
	end
end
