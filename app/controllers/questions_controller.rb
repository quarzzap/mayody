class QuestionsController < ApplicationController
	def show
		@question = Question.find(params[:id])
	end

	def new
		@question = Question.new
		#@active_options = []
		for i in 0..3
			@question.options.build()
		end
	end

	def create
		@question = Question.new(question_params)
		if @question.save 
			flash[:success] = "question created"
			render text: question_params.to_s + '\n' + params.to_s
		else 
			render 'new'
		end
	end

	private
		def question_params
			params.require(:question).permit!
		end
end
