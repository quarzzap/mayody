class TestQuestionsController < ApplicationController
	def new
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.find(params[:test_lesson_id])
		@test_question = @test_lesson.test_questions.build
		for i in 0..3
			@test_question.question_options.build
		end
	end

	def create
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.find(params[:test_lesson_id])
		@test_question = @test_lesson.test_questions.build(test_question_params)
		if @test_question.save			
			flash[:success] = "question created"
			redirect_to [@course, @chapter, @test_lesson]
		else 
			render 'new'
		end
	end
	
	private
		def test_question_params
			params.require(:test_question).permit!
		end
end
