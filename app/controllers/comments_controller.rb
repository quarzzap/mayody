class CommentsController < ApplicationController
  def new
  	@comment = Comment.new
  end

  def create
  	@comment = Comment.new(comment_params)
  	@test = Test.find(params[:test_id])
  	@comment.test = @test
  	@comment.user = current_user
  	@comment.save
  	redirect_to @test
  end

  private 
  	def comment_params
  		params.require(:comment).permit(:text)
  	end
end
