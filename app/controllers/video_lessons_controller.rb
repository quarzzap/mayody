class VideoLessonsController < ApplicationController

	def show
		@course = Course.find(params[:course_id])
		@chapter = Chapter.find(params[:chapter_id])
		@video_lesson = @chapter.video_lessons.find(params[:id])
	end

	def new
		@course = Course.find(params[:course_id])
		@chapter = Chapter.find(params[:chapter_id])
		@video_lesson = @chapter.video_lessons.build
	end

	def create
		@course = Course.find(params[:course_id])
		@chapter = Chapter.find(params[:chapter_id])
		@video_lesson = @chapter.video_lessons.build(video_lesson_params)
		if (@video_lesson.save)
			redirect_to [@course, @chapter]
		else 
			render 'new'
		end	
	end

	private
		def video_lesson_params
			params.require(:video_lesson).permit(:name, :description, :src)
		end
end
