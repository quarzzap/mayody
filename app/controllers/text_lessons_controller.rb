class TextLessonsController < ApplicationController
	def show
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@text_lesson = @chapter.text_lessons.find(params[:id])
	end

	def new 
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@text_lesson = @chapter.text_lessons.build
	end

	def create 
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@text_lesson = @chapter.text_lessons.build(text_lesson_params)
		if @text_lesson.save
			redirect_to [@course, @chapter]
		else
			render 'new'
		end
	end

	private
		def text_lesson_params
			params.require(:text_lesson).permit(:name, :description, :text)
		end
end
