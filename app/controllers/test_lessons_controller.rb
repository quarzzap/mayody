class TestLessonsController < ApplicationController

	def show
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.find(params[:id])
	end

	def new
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.build
	end

	def create
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.build(test_lesson_params)
		if @test_lesson.save
			redirect_to [@course, @chapter]
		else
			render 'new'
		end
	end

	def run
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.find(params[:id])
		if begin?

		else
			session["tests#{@test_lesson.id}"] = []
		end

		@question_index = session["tests#{@test_lesson.id}"].length

		if @question_index < @test_lesson.test_questions.to_a.length
			@test_question = @test_lesson.test_questions.to_a[@question_index]
			render "question"
		else 
			render "finish"
		end
	end

	def save_answer
		@course = Course.find(params[:course_id])
		@chapter = @course.chapters.find(params[:chapter_id])
		@test_lesson = @chapter.test_lessons.find(params[:id])
		@number = session["tests#{@test_lesson.id}"].length
		@answer = Integer(params[:selected])
		session["tests#{@test_lesson.id}"][@number] = @answer
		run
	end

	def begin?
		not session["tests#{@test_lesson.id}"].nil?
	end

	private
		def test_lesson_params
			params.require(:test_lesson).permit(:name, :description)
		end
end
