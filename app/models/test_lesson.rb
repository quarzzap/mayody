class TestLesson < ActiveRecord::Base
  belongs_to :chapter
  has_many :test_questions
end
