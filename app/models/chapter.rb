class Chapter < ActiveRecord::Base
	belongs_to :course
	has_many :text_lessons
	has_many :test_lessons
	has_many :video_lessons
end
