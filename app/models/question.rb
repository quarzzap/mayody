class Question < ActiveRecord::Base
	has_many :options
	has_and_belongs_to_many :tests

	accepts_nested_attributes_for :options
end
