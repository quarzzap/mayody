 class Test < ActiveRecord::Base
	has_and_belongs_to_many :questions
	has_many :comments

	serialize :selected_options
end
