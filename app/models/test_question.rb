class TestQuestion < ActiveRecord::Base
  belongs_to :test_lesson
  has_many :question_options

  accepts_nested_attributes_for :question_options
end
