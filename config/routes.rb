Rails.application.routes.draw do

  get 'comments/new'

  get 'sessions/new'

	root 'static_pages#home'
  get 'help' => 'static_pages#help'
  get 'about' => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup' => 'users#new' 
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get '/tests/:id/start' => 'tests#start'
  post '/tests/:id/start' => 'test#start'
  get '/tests/:id/run/:number' => 'tests#question'
  post '/tests/:id/run/:number' => 'tests#save_answer'
  get '/tests/:id/done' => 'tests#finish'
  get '/courses/:course_id/chapters/:chapter_id/test_lessons/:id/run' => 'test_lessons#run'
  post '/courses/:course_id/chapters/:chapter_id/test_lessons/:id/run' => 'test_lessons#save_answer'

  resources :users
  resources :questions
  resources :tests do
    resources :comments
  end
  resources :courses do
    resources :chapters do
      resources :text_lessons
      resources :test_lessons do
        resources :test_questions
      end
      resources :video_lessons
    end
  end
end
