# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519183809) do

  create_table "chapters", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "course_id"
  end

  add_index "chapters", ["course_id"], name: "index_chapters_on_course_id"

  create_table "comments", force: :cascade do |t|
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "test_id"
  end

  add_index "comments", ["test_id"], name: "index_comments_on_test_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "options", force: :cascade do |t|
    t.string   "text"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "question_id"
  end

  add_index "options", ["question_id"], name: "index_options_on_question_id"

  create_table "question_options", force: :cascade do |t|
    t.text     "text"
    t.integer  "test_question_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "question_options", ["test_question_id"], name: "index_question_options_on_test_question_id"

  create_table "questions", force: :cascade do |t|
    t.string   "title"
    t.text     "text"
    t.integer  "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions_tests", id: false, force: :cascade do |t|
    t.integer "question_id", null: false
    t.integer "test_id",     null: false
  end

  add_index "questions_tests", ["question_id", "test_id"], name: "index_questions_tests_on_question_id_and_test_id"
  add_index "questions_tests", ["test_id", "question_id"], name: "index_questions_tests_on_test_id_and_question_id"

  create_table "test_lessons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "chapter_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "test_lessons", ["chapter_id"], name: "index_test_lessons_on_chapter_id"

  create_table "test_questions", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "test_lesson_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "right_answer"
  end

  add_index "test_questions", ["test_lesson_id"], name: "index_test_questions_on_test_lesson_id"

  create_table "tests", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "text_lessons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "text"
    t.integer  "chapter_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "text_lessons", ["chapter_id"], name: "index_text_lessons_on_chapter_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

  create_table "video_lessons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "src"
    t.integer  "chapter_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "video_lessons", ["chapter_id"], name: "index_video_lessons_on_chapter_id"

end
