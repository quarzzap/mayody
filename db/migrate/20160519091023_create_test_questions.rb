class CreateTestQuestions < ActiveRecord::Migration
  def change
    create_table :test_questions do |t|
      t.string :name
      t.text :text
      t.references :test_lesson, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
