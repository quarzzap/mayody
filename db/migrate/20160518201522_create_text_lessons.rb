class CreateTextLessons < ActiveRecord::Migration
  def change
    create_table :text_lessons do |t|
      t.string :name
      t.text :description
      t.text :text
      t.references :chapter, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
