class AddRightAnswerToTestQuestion < ActiveRecord::Migration
  def change
  	change_table :test_questions do |t|
  		t.integer :right_answer
  	end
  end
end
