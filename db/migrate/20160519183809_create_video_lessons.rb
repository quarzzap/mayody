class CreateVideoLessons < ActiveRecord::Migration
  def change
    create_table :video_lessons do |t|
      t.string :name
      t.text :description
      t.text :src
      t.references :chapter, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
