class CreateQuestionOptions < ActiveRecord::Migration
  def change
    create_table :question_options do |t|
      t.text :text
      t.references :test_question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
