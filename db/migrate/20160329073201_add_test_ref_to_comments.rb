class AddTestRefToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :test, index: true, foreign_key: true
  end
end
